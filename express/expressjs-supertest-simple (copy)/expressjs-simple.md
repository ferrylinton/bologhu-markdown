# Contoh ExpressJS Sederhana

Membuat alplikasi REST sederhana, dengan API Get untuk menampilkan data JSON yang sederhana. Projek akan menggunakan konfigurasi ExpressJS yang sederhana.

## Langkah-Langkah Yang Digunakan

### 1. Buat projek NodeJS

```
npm init -y
```

### 2. Library yang digunakan

- [Express](https://expressjs.com/)
- [Supertest](https://www.npmjs.com/package/supertest)
- [Istanbul](https://istanbul.js.org/)

### 3. Struktur proyek yang akan digunakan

```
.
├── index.js
├── package.json
├── README.md
├── src
│   ├── app.js
│   └── routes
│       └── simple-route.js
└── test
    └── routes
        └── simple-route.spec.js
```

### 4. Buat berkas **simple-route.js**

Tulis kode berikut

```js
const express = require('express');

var router = express.Router();                // (1)
router.get('/', getData);                     // (2)

async function getData(req, res, next) {
    let data = {                              // (3)
        name: 'Ferry L. H.'
    }

    res.status(200).json(data);               // (4)
}

module.exports = router;
```

Deskripsi:

> (1) Handler untuk route
>
> (2) Mendaftarkan fungsi **getData** untuk link **/**
>
> (3) Data yang akan ditampilkan di **response**
>
> (4) Response akan ditampilkan dalam bentuk **json** 

### 5. Buat berkas **app.js**

Tulis kode berikut

```js
const express = require('express');
const simpleRoute = require('./routes/simple-route');


const app = express();
app.use('/', simpleRoute);

module.exports = app;
```

### 6. Buat berkas **index.js**

Tulis kode berikut

```js
const app = require('./src/app');
const port = parseInt(process.env.PORT || '3000', 10);


app.listen(port, function () {
    console.log(`####################################################################`)
    console.log(`NODE_ENV   : ${process.env.NODE_ENV}`);
    console.log(`address    : ${JSON.stringify(this.address())}`);
    console.log(`####################################################################`)
});
```

### 7. Ubah berkas **package.json**

Tulis kode berikut

```json
{
  "name": "expressjs-simple",
  "version": "1.0.0",
  "description": "Starting With ExpressJS",
  "main": "index.js",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/ferrylinton/bologhu-code.git"
  },
  "scripts": {
    "start": "NODE_ENV=development node index.js"
  },
  "keywords": [
    "expressjs",
    "supertest",
    "nyc"
  ],
  "author": "Ferry L. H.",
  "license": "ISC",
  "dependencies": {
    "express": "^4.17.1"
  },
  "devDependencies": {
    "nyc": "^15.1.0",
    "supertest": "^6.1.3"
  }
}
```

### 8. Jalankan projek

```
npm start
```

![expressjs-simple-1.png](images/expressjs-simple-1.png)

### 6. Buka browser dan dengan alamat **http://localhost:3000**

Browser akan menampilkan teks berikut:

```
Horas!
```

![expressjs-hello-world-2.png](images/expressjs-hello-world-2.png)

## Source code

1. [Github](https://github.com/ferrylinton/bologhu-code/tree/master/expressjs/expressjs-simple)
2. [Gitlab](https://gitlab.com/ferrylinton/bologhu-code/-/tree/master/expressjs/expressjs-simple)
3. [Bitbucket](https://bitbucket.org/ferrylinton/bologhu-code/src/master/expressjs/expressjs-simple/)

